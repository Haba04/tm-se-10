package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskRemoveOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "remove all tasks";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = serviceLocator.getScanner().nextLine();
        @Nullable final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().remove(userId, taskId);
        System.out.println("TASK REMOVED SUCCESSFULLY");
    }
}
