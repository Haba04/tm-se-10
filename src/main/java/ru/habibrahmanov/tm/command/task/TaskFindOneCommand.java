package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Task;

public final class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find";
    }

    @Override
    public String getDescription() {
        return "find task by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = serviceLocator.getScanner().nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findOne(taskId, serviceLocator.getUserService().getCurrentUser().getId());
        System.out.println("TASK ID: " + task.getId() + " / NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("DATE BEGIN: " + task.getDateBegin() + " / DATE END: " + task.getDateEnd());
        System.out.println();
    }
}
