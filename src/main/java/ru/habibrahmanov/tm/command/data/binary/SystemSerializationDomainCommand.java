package ru.habibrahmanov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SystemSerializationDomainCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "serialization-domain";
    }

    @Override
    public String getDescription() {
        return "saving a domain using serialization";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final FileOutputStream userFileOutputStream = new FileOutputStream("src\\main\\file\\userFile");
        @NotNull final ObjectOutputStream userObjectOutputStream = new ObjectOutputStream(userFileOutputStream);
        userObjectOutputStream.writeObject(serviceLocator.getUserService().findAll());
        userFileOutputStream.close();
        userObjectOutputStream.close();

        @NotNull final FileOutputStream taskFileOutputStream = new FileOutputStream("src\\main\\file\\taskFile");
        @NotNull final ObjectOutputStream taskObjectOutputStream = new ObjectOutputStream(taskFileOutputStream);
        taskObjectOutputStream.writeObject(serviceLocator.getTaskService().findAll());
        taskFileOutputStream.close();
        taskObjectOutputStream.close();

        @NotNull final FileOutputStream projectFileOutputStream = new FileOutputStream("src\\main\\file\\projectFile");
        @NotNull final ObjectOutputStream projectObjectOutputStream = new ObjectOutputStream(projectFileOutputStream);
        projectObjectOutputStream.writeObject(serviceLocator.getProjectService().findAll());
        projectFileOutputStream.close();
        projectObjectOutputStream.close();

        System.out.println("OK");
    }
}
