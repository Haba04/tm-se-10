package ru.habibrahmanov.tm.command.data.json;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.wrapper.ProjectList;
import ru.habibrahmanov.tm.wrapper.TaskList;
import ru.habibrahmanov.tm.wrapper.UserList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class SystemLoadJaxbJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jaxb-json";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jaxb technology in json transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Unmarshaller projectUnmarshaller = projectJaxbContext.createUnmarshaller();
        projectUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        projectUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        @NotNull final ProjectList projectList = (ProjectList) projectUnmarshaller
                .unmarshal(new File("src\\main\\file\\projectList.json"));
        for (Project project : projectList.getProjectList()) {
            serviceLocator.getProjectService().persist(project);
        }

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Unmarshaller taskUnmarshaller = taskJaxbContext.createUnmarshaller();
        taskUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        taskUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        @NotNull final TaskList taskList = (TaskList) taskUnmarshaller
                .unmarshal(new File("src\\main\\file\\taskList.json"));
        for (Task task : taskList.getTaskList()) {
            serviceLocator.getTaskService().persist(task);
        }

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Unmarshaller userUnmarshaller = userJaxbContext.createUnmarshaller();
        userUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        userUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        @NotNull final UserList userList = (UserList) userUnmarshaller
                .unmarshal(new File("src\\main\\file\\userList.json"));
        for (User user : userList.getUserList()) {
            serviceLocator.getUserService().registryUser(user);
        }

        System.out.println("OK");
    }
}
