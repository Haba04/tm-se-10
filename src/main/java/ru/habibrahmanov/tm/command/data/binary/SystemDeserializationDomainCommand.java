package ru.habibrahmanov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Collection;

public class SystemDeserializationDomainCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "deserialization-domain";
    }

    @Override
    public String getDescription() {
        return "loading a domain using serialization";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final FileInputStream userFileInputStream = new FileInputStream("src\\main\\file\\userFile");
        @NotNull final ObjectInputStream userObjectInputStream = new ObjectInputStream(userFileInputStream);
        @NotNull final Collection<User> userCollection = (Collection<User>) userObjectInputStream.readObject();
        for (User user : userCollection) {
            serviceLocator.getUserService().registryUser(user);
        }
        userFileInputStream.close();
        userObjectInputStream.close();

        @NotNull final FileInputStream taskFileInputStream = new FileInputStream("src\\main\\file\\taskFile");
        @NotNull final ObjectInputStream taskObjectInputStream = new ObjectInputStream(taskFileInputStream);
        @NotNull final Collection<Task> taskCollection = (Collection<Task>) taskObjectInputStream.readObject();
        for (Task task : taskCollection) {
            serviceLocator.getTaskService().persist(task);
        }
        taskFileInputStream.close();
        taskObjectInputStream.close();

        final FileInputStream projectFileInputStream = new FileInputStream("src\\main\\file\\projectFile");
        final ObjectInputStream projectObjectInputStream = new ObjectInputStream(projectFileInputStream);
        @NotNull final Collection<Project> projectCollection = (Collection<Project>) projectObjectInputStream.readObject();
        for (Project project : projectCollection) {
            serviceLocator.getProjectService().persist(project);
        }
        projectFileInputStream.close();
        projectObjectInputStream.close();

        System.out.println("OK");
    }
}
