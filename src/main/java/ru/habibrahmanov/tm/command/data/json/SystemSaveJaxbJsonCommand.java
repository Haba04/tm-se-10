package ru.habibrahmanov.tm.command.data.json;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.wrapper.ProjectList;
import ru.habibrahmanov.tm.wrapper.TaskList;
import ru.habibrahmanov.tm.wrapper.UserList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class SystemSaveJaxbJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jaxb-json";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jaxb technology in json transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Marshaller projectMarshaller = projectJaxbContext.createMarshaller();
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        projectMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        projectMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        @NotNull final ProjectList projectList = new ProjectList();
        projectList.setProjectList(serviceLocator.getProjectService().findAll());
        projectMarshaller.marshal(projectList, new File("src/main/file/projectList.json"));

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Marshaller taskMarshaller = taskJaxbContext.createMarshaller();
        taskMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        taskMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().findAll());
        taskMarshaller.marshal(taskList, new File("src\\main\\file\\taskList.json"));

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Marshaller userMarshaller = userJaxbContext.createMarshaller();
        userMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        userMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final UserList userList = new UserList();
        userList.setUserList(serviceLocator.getUserService().findAll());
        userMarshaller.marshal(userList, new File("src\\main\\file\\userList.json"));

        System.out.println("OK");
    }
}
