package ru.habibrahmanov.tm.command.data.xml;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;

import java.io.File;
import java.util.Iterator;

public class SystemLoadJacksonXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jackson-xml";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jackson technology in xml transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JacksonXmlModule jacksonXmlModule = new JacksonXmlModule();
        jacksonXmlModule.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(jacksonXmlModule);

        @NotNull final Iterator<Project> projectIterator = xmlMapper.readerFor(Project.class).readValues(new File("src\\main\\file\\projectJackson.xml"));
        while (projectIterator.hasNext()) {
            serviceLocator.getProjectService().persist(projectIterator.next());
        }

        @NotNull final Iterator<Task> taskIterator = xmlMapper.readerFor(Task.class).readValues(new File("src\\main\\file\\taskJackson.xml"));
        while (taskIterator.hasNext()) {
            serviceLocator.getTaskService().persist(taskIterator.next());
        }

        @NotNull final Iterator<User> userIterator = xmlMapper.readerFor(User.class).readValues(new File("src\\main\\file\\userJackson.xml"));
        while (userIterator.hasNext()) {
            serviceLocator.getUserService().registryUser(userIterator.next());
        }

        System.out.println("OK");
    }
}
