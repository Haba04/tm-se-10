package ru.habibrahmanov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.wrapper.ProjectList;
import ru.habibrahmanov.tm.wrapper.TaskList;
import ru.habibrahmanov.tm.wrapper.UserList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class SystemSaveJaxbXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jaxb-xml";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jaxb technology in xml transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ProjectList projectList = new ProjectList();
        @NotNull final TaskList taskList = new TaskList();
        @NotNull final UserList userList = new UserList();

        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Marshaller projectMarshaller = projectJaxbContext.createMarshaller();
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        projectList.setProjectList(serviceLocator.getProjectService().findAll());
        projectMarshaller.marshal(projectList, new File("src\\main\\file\\projectList.xml"));

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Marshaller taskMarshaller = taskJaxbContext.createMarshaller();
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        taskList.setTaskList(serviceLocator.getTaskService().findAll());
        taskMarshaller.marshal(taskList, new File("src\\main\\file\\taskList.xml"));

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Marshaller userMarshaller = userJaxbContext.createMarshaller();
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        userList.setUserList(serviceLocator.getUserService().findAll());
        userMarshaller.marshal(userList, new File("src\\main\\file\\userList.xml"));

        System.out.println("OK");
    }
}
