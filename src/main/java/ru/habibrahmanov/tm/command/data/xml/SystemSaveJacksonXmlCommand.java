package ru.habibrahmanov.tm.command.data.xml;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;

import java.io.FileOutputStream;

public class SystemSaveJacksonXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jackson-xml";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jackson technology in xml transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);

        @NotNull final FileOutputStream projectFileOutputStream = new FileOutputStream("src\\main\\file\\projectJackson.xml");
        @NotNull final String projectJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getProjectService().findAll());
        projectFileOutputStream.write(projectJackson.getBytes());

        @NotNull final FileOutputStream taskFileOutputStream = new FileOutputStream("src\\main\\file\\taskJackson.xml");
        @NotNull final String taskJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getTaskService().findAll());
        taskFileOutputStream.write(taskJackson.getBytes());

        @NotNull FileOutputStream userFileOutputStream = new FileOutputStream("src\\main\\file\\userJackson.xml");
        @NotNull final String userJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getUserService().findAll());
        userFileOutputStream.write(userJackson.getBytes());

        System.out.println("OK");
    }
}
