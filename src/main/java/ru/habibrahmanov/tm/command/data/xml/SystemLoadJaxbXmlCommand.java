package ru.habibrahmanov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.wrapper.ProjectList;
import ru.habibrahmanov.tm.wrapper.TaskList;
import ru.habibrahmanov.tm.wrapper.UserList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class SystemLoadJaxbXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jaxb-xml";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jaxb technology in xml transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Unmarshaller projectUnmarshaller = projectJaxbContext.createUnmarshaller();
        @NotNull final ProjectList projectList = (ProjectList) projectUnmarshaller.unmarshal(new File("src\\main\\file\\projectList.xml"));
        for (Project project : projectList.getProjectList()) {
            serviceLocator.getProjectService().persist(project);
        }

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Unmarshaller taskUnmarshaller = taskJaxbContext.createUnmarshaller();
        @NotNull final TaskList taskList = (TaskList) taskUnmarshaller.unmarshal(new File("src\\main\\file\\taskList.xml"));
        for (Task task : taskList.getTaskList()) {
            serviceLocator.getTaskService().persist(task);
        }

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Unmarshaller userUnmarshaller = userJaxbContext.createUnmarshaller();
        @NotNull final UserList userList = (UserList) userUnmarshaller.unmarshal(new File("src\\main\\file\\userList.xml"));
        for (User user : userList.getUserList()) {
            serviceLocator.getUserService().registryUser(user);
        }

        System.out.println("OK");
    }
}