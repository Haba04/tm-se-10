package ru.habibrahmanov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.wrapper.ProjectList;
import ru.habibrahmanov.tm.wrapper.TaskList;
import ru.habibrahmanov.tm.wrapper.UserList;

import java.io.File;

public class SystemLoadJacksonJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jackson-json";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jackson technology in json transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final UserList userList = userMapper.readValue(new File("src\\main\\file\\userJackson.json"), UserList.class);
        for (User user : userList.getUserList()) {
            serviceLocator.getUserService().registryUser(user);
        }

        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final ProjectList projectList = projectMapper.readValue(new File("src\\main\\file\\projectJackson.json"), ProjectList.class);
        for (Project project : projectList.getProjectList()) {
            serviceLocator.getProjectService().persist(project);
        }

        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final TaskList taskList = taskMapper.readValue(new File("src\\main\\file\\taskJackson.json"), TaskList.class);
        for (Task task : taskList.getTaskList()) {
            serviceLocator.getTaskService().persist(task);
        }

        System.out.println("OK");
    }
}
