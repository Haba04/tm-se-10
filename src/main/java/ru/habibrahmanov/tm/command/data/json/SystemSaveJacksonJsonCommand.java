package ru.habibrahmanov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.wrapper.ProjectList;
import ru.habibrahmanov.tm.wrapper.TaskList;
import ru.habibrahmanov.tm.wrapper.UserList;

import java.io.File;

public class SystemSaveJacksonJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jackson-json";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jackson technology in json transport format";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final UserList userList = new UserList();
        userList.setUserList(serviceLocator.getUserService().findAll());
        userMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src\\main\\file\\userJackson.json"), userList);

        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final ProjectList projectList = new ProjectList();
        projectList.setProjectList(serviceLocator.getProjectService().findAll());
        projectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src\\main\\file\\projectJackson.json"), projectList);

        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().findAll());
        taskMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src\\main\\file\\taskJackson.json"), taskList);

        System.out.println("OK");
    }
}
