package ru.habibrahmanov.tm.command;

import ru.habibrahmanov.tm.api.ServiceLocator;

import java.text.SimpleDateFormat;

public abstract class AbstractCommand {
        protected ServiceLocator serviceLocator;
        public final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        public void setBootstrap(ServiceLocator serviceLocator) {
            this.serviceLocator = serviceLocator;
        }
        public abstract String getName();
        public abstract String getDescription();
        public abstract boolean secure();
        public abstract void execute() throws Exception;
}

