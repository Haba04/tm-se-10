package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectRemoveOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "remove project by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER ID PROJECT:");
        @NotNull final String projectId = serviceLocator.getScanner().nextLine();
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().remove(projectId, userId);
        serviceLocator.getTaskService().removeAll(projectId, userId);
        System.out.println("PROJECT REMOVED SUCCESSFULLY");
    }
}
