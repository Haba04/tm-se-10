package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;

public class ProjectFindByAddingCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-find-adding";
    }

    @Override
    public String getDescription() {
        return "shows all projects in the order they were added";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND BY ADDING]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().findByAdding(serviceLocator.getUserService().getCurrentUser().getId());
        projectList.forEach((v) -> System.out.println("PROJECT ID: " + v.getId() + " / PROJECT NAME: " + v.getName()));

    }
}
