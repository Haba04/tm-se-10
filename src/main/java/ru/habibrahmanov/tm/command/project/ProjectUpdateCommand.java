package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "update project by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER NAME TO CHANGE");
        @Nullable final String name = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE BEGIN:");
        @Nullable final String dateBegin = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE END:");
        @Nullable final String dateEnd = serviceLocator.getScanner().nextLine();
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().update(userId, projectId, name, description, dateBegin, dateEnd);
        System.out.println("PROJECT EDIT SUCCESSFULLY");
    }
}
