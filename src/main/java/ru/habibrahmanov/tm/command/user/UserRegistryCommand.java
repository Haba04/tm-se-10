package ru.habibrahmanov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Role;
import ru.habibrahmanov.tm.util.HashUtil;

public final class UserRegistryCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public String getDescription() {
        return "new user registration";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE NEW USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = serviceLocator.getScanner().nextLine();
        serviceLocator.getUserService().registryUser(new User(login, HashUtil.md5(password), Role.USER));
        System.out.println("CREATE NEW USER SUCCESSFULLY");
    }
}
