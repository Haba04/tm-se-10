package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class UserViewProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VIEW PROFILE]");
        serviceLocator.getUserService().viewProfile();
        System.out.println("your login: " + serviceLocator.getUserService().viewProfile().getLogin());
        System.out.println("your password: " + serviceLocator.getUserService().viewProfile().getPassword());
    }
}
