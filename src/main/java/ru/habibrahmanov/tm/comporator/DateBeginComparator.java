package ru.habibrahmanov.tm.comporator;

import ru.habibrahmanov.tm.entity.AbstractEntity;
import java.util.Comparator;

public final class DateBeginComparator<T> implements Comparator<AbstractEntity> {
    @Override
    public int compare(AbstractEntity o1, AbstractEntity o2) {
        return o1.getDateBegin().compareTo(o2.getDateBegin());
    }
}