package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.habibrahmanov.tm.enumeration.Status;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractEntity {
    protected String id;
    protected String userId;
    protected String name;
    protected String description;
    protected Date dateBegin;
    protected Date dateEnd;
    protected Status status = Status.PLANNED;
}
