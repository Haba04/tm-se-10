package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.habibrahmanov.tm.enumeration.Role;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public final class User implements Serializable {
    private String id = UUID.randomUUID().toString();
    private String login;
    private String password;
    private Role role;

    public User() {
    }

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

}
