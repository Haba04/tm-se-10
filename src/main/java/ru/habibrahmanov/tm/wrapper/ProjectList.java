package ru.habibrahmanov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "projectList")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectList {
    @Nullable
    @XmlElement(name = "project")
    private List<Project> projectList = null;

    @Nullable
    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }
}
