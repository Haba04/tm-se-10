package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.bootstrap.Bootstrap;

public class Application {
    public static void main(String[] args) throws Exception {
        System.setProperty("javax.xml.bind.JAXBContextFactory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
